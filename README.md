# CRM_Service_E-commerce
Mini project CRM Service datbase for e-commerce using MySQL.

[![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)](https://dev.mysql.com/doc/)

Powered with MySQL.

Demo to implement RDBMS with DDL, DML and DCL.

Entitiy Relationship Diagram :
![ERD](CRM_ServiceDiagram1.png)